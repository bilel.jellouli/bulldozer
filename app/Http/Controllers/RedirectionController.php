<?php

namespace App\Http\Controllers;

use App\Models\Link;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Stevebauman\Location\Facades\Location;

class RedirectionController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  Request  $request
     * @param Link
     * @return Redirect
     */
    public function __invoke(Request $request, Link $link)
    {
        $data = [
            'access_time' => date('Y-m-d H:i:s'),
            'link' => $link->url,
            'ip' => $request->ip(),
            'user_agent' => $request->header('User-Agent')
        ];

        try {
            $position = Location::get();
            $data['country'] = $position->countryName;
        } catch (\Exception $e) {
            // Could not get the country
        }

        if (auth()->check()) {
            $data['user_id'] = auth()->id();
        }

        Log::info('URL_REACHED', $data);

        return redirect()->away($link->url);
    }
}
