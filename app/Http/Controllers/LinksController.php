<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLinkRequest;
use App\Jobs\AutoDelete;
use App\Models\Link;
use Illuminate\Support\Str;

class LinksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('links.index', [
            'links' => auth()->user()->links
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('links.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateLinkRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateLinkRequest $request)
    {

        do {
            $code = Str::random(7);
        } while (Link::withTrashed()->where('code', $code)->exists());

        if (auth()->user()->links->count() === 5) {
            auth()->user()->links()->orderBy('created_at')->first()->delete();
        }

        if (Link::count() === 20) {
            Link::orderBy('created')->first()->delete();
        }

        $link = auth()->user()->links()->create([
            'code' => (string)$code,
            'url' => $request->url
        ]);

        AutoDelete::dispatch($link)->delay($link->created_at->addDay());

        return redirect(route('links.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function destroy(Link $link)
    {
        if (auth()->id() !== $link->user_id) {
            abort(403);
        }

        $link->delete();

        return redirect(route('links.index'));
    }
}
