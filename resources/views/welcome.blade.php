<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bulldozer</title>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <style>
        body {
            background-color: #eaeaea;
            background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 80 80' width='80' height='80'%3E%3Cpath fill='%23dfd7ec' fill-opacity='0.4' d='M14 16H9v-2h5V9.87a4 4 0 1 1 2 0V14h5v2h-5v15.95A10 10 0 0 0 23.66 27l-3.46-2 8.2-2.2-2.9 5a12 12 0 0 1-21 0l-2.89-5 8.2 2.2-3.47 2A10 10 0 0 0 14 31.95V16zm40 40h-5v-2h5v-4.13a4 4 0 1 1 2 0V54h5v2h-5v15.95A10 10 0 0 0 63.66 67l-3.47-2 8.2-2.2-2.88 5a12 12 0 0 1-21.02 0l-2.88-5 8.2 2.2-3.47 2A10 10 0 0 0 54 71.95V56zm-39 6a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm40-40a2 2 0 1 1 0-4 2 2 0 0 1 0 4zM15 8a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm40 40a2 2 0 1 0 0-4 2 2 0 0 0 0 4z'%3E%3C/path%3E%3C/svg%3E");
        }

    </style>
</head>

<body>
    <div class="w-screen h-screen flex flex-col justify-between items-center">
        <header class="p-4 flex justify-end w-full">
            <nav>
                <ul class="flex gap-2">
                    @guest
                        <li><a class="text-indigo-900 hover:text-indigo-400"
                                href="{{ route('login') }}">{{ __('Login') }}</a></li>
                        <li><a class="text-indigo-900 hover:text-indigo-400"
                                href="{{ route('register') }}">{{ __('Register') }}</a></li>
                    @else
                        <li><a class="text-indigo-900 hover:text-indigo-400"
                                href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a></li>
                    @endguest
                </ul>
            </nav>
        </header>
        <main class="text-gray-600 flex flex-col justify-center">
            <div class="flex items-center justify-center">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-14 w-14 fill-current text-indigo-500"
                    viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd"
                        d="M12.586 4.586a2 2 0 112.828 2.828l-3 3a2 2 0 01-2.828 0 1 1 0 00-1.414 1.414 4 4 0 005.656 0l3-3a4 4 0 00-5.656-5.656l-1.5 1.5a1 1 0 101.414 1.414l1.5-1.5zm-5 5a2 2 0 012.828 0 1 1 0 101.414-1.414 4 4 0 00-5.656 0l-3 3a4 4 0 105.656 5.656l1.5-1.5a1 1 0 10-1.414-1.414l-1.5 1.5a2 2 0 11-2.828-2.828l3-3z"
                        clip-rule="evenodd" />
                </svg>
                <span class="text-4xl">Bulldozer</span>
            </div>
            <h2>{{ __('Create short and awesome urls') }}</h2>
        </main>
        <footer class="bg-slate-100/50 p-4 w-full text-center text-gray-600">
            Bulldozer {{ __('all copy right reserved') }} &copy; {{ date('Y') }}
        </footer>
    </div>
</body>

</html>
