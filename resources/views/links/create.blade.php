<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="POST" action="{{ route('links.store') }}">
                        @csrf
                        <div class="flex flex-col gap-y-4">
                            <div class="mb-4">
                                <label class="block mb-2" for="url">Url</label>
                                <input id="url" name="url" class="w-full @error('url') border-red-500 @enderror"
                                    type="text" placeholder="https://yourdomain.com/your-url"
                                    value="{{ old('url') }}">
                                @error('url')
                                    <span class="text-red-500">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="flex justify-end">
                                <button type="submit"
                                    class="rounded text-white px-6 py-2 bg-indigo-700 hover:bg-indigo-400">
                                    {{ __('Create') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
