<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('My links') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @forelse ($links as $link)
                        <div class="flex justify-between items-center border-b border-gray-500 py-2">
                            <div class="flex-grow">
                                <p><a href="{{ $link->url }}" target="_blank">{{ $link->url }}</a></p>
                                <span class="text-gray-800">{{ route('redirector', ['link' => $link->code]) }}</span>
                            </div>
                            <div>
                                <form action="{{ route('links.destroy', $link->id) }}" method="POST"
                                    onsubmit="if(!confirm('are you sure you want to delete this link')){return false}">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit"
                                        class="inline-block my-4 rounded text-white px-6 py-1 text-sm bg-red-700 hover:bg-red-400">{{ __('Delete') }}</button>
                                </form>
                            </div>
                        </div>
                    @empty
                        <div
                            class="p-4 m-4 w-full border border-dashed border-gray-200 rounded flex flex-col items-center justify-center">
                            <h3 class="text-gray-700 text-lg">{{ __('No links found') }}</h3>
                            <a href="{{ route('links.create') }}"
                                class="my-4 rounded text-white px-6 py-2 bg-indigo-700 hover:bg-indigo-400">{{ __('Create') }}</a>
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
