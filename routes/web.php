<?php

use App\Http\Controllers\LinksController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RedirectionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::resource('links', LinksController::class)->only(['index', 'create', 'store', 'destroy'])->middleware(['auth']);

require __DIR__ . '/auth.php';


Route::get('{link:code}', RedirectionController::class)->name('redirector');
